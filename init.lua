
fsr = {}

local MP = minetest.get_modpath("fsr_custom")

-- utilities
dofile(MP.."/util.lua")

-- some items don't have recipes. fill them in
dofile(MP.."/missing_recipes.lua")

-- make some advtrains recipes craftable from survival
dofile(MP.."/advtrains/survival_recipes.lua")
-- make trains ignore microblocks and some others
dofile(MP.."/advtrains/ignore_collisions.lua")
-- make trains stop some time after last player left server
dofile(MP.."/advtrains/stop_on_inactivity.lua")

-- warning message for default password (if set)
if minetest.settings:get("default_password") then
    dofile(MP.."/default_password_warn.lua")
end

-- /spawn command
dofile(MP.."/chat/spawn.lua")

-- by popular demand
dofile(MP.."/additional_stuff/salad_pickaxe.lua")
