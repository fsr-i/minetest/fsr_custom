
minetest.register_tool("fsr_custom:pick_salad", {
	description = "Pickaxe of questionable quality",
	inventory_image = "fsr_custom_tool_picksalad.png",
	stack_max = 1,
	sound = {breaks = "fsr_custom_salad_splotch"},
	on_use = function(itemstack, user, pointed_thing)
		if user ~= nil then
			minetest.sound_play({
				name = "fsr_custom_salad_splotch",
			}, {
				object = user
			})
		end
		return ItemStack()
	end
})

fsr.register_craft_if_items_exist({
    output = "fsr_custom:pick_salad",
    recipe = {
        {"farming:potato_salad", "farming:potato_salad", "farming:potato_salad"},
        {"", "default:stick", ""},
        {"", "default:stick", ""}
    }
})
