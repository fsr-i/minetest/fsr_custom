-- add blocks to be ignored by trains

local additional_nonblocknodes={
    "digiterms:lcd_monitor",
    "digiterms:beige_keyboard",
    "digiterms:white_keyboard",
    "digiterms:black_keyboard",
    "digilines:lcd",
    "homedecor:glowlight_quarter_14",
    "mesecons_pressureplates:pressure_plate_wood",
    "mesecons_pressureplates:pressure_plate_stone",
}

local mods = {"moreblocks", "bakedclay", "building_blocks", "my_door_wood", "my_sliding_doors"}
local variants = {
    micro_ = nil,
    slope = nil,
    slab_ = {"_1", "_2", "_quarter", "_two_sides", "_three_sides"},
    stair_ = {"_alt", "_alt_1", "_alt_2", "_alt_4", "_outer"},
    panel_ = {"_1", "_2", "_4"},
}
local to_add = {}
for i, mod in ipairs(mods) do
    for variant, data in pairs(variants) do
        if to_add[mod] == nil then
            to_add[mod] = {}
        end
        to_add[mod][variant] = data
    end
end
for name, def in pairs(minetest.registered_nodes) do
    for mod, variants in pairs(to_add) do
        if string.sub(name, 1, #mod+1) == mod .. ":" then
            for variant, endings in pairs(variants) do
                local prefix = mod .. ":" .. variant
                if string.sub(name, 1, #prefix) == prefix then
                    if endings then
                        for _, ending in ipairs(endings) do
                            if string.sub(name, #name - #ending + 1, #name) == ending then
                                table.insert(additional_nonblocknodes, name)
                            end
                        end
                    else
                        table.insert(additional_nonblocknodes, name)
                    end
                end
            end
        end
    end
end

for _,name in ipairs(additional_nonblocknodes) do
    if minetest.registered_nodes[name] then
        minetest.registered_nodes[name].groups.not_blocking_trains=1
    end
end
