local greens = {"dye:green", "dye:light_green", "dye:medium_green", "dye:dark_green"}


-- track stuff

fsr.register_craft_if_items_exist({
    output = "advtrains_interlocking:tcb_node",
    recipe = {{"default:mese_crystal_fragment"},
              {"default:steel_ingot"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains_interlocking:tcb_node 9",
    recipe = {{"default:mese_crystal"},
              {"default:steelblock"}}
    })

fsr.register_craft_if_items_exist({
    output = "advtrains_line_automation:dtrack_stop_placer",
    recipe = {{"dye:black"},
              {"default:mese_crystal_fragment"},
              {"advtrains:dtrack_placer"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains_interlocking:dtrack_npr_placer",
    recipe = {{"advtrains_line_automation:dtrack_stop_placer"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains_interlocking:dtrack_npr_placer",
    recipe = {{"advtrains_line_automation:dtrack_stop_placer"}}
    })

--[[
-- TODO: fix crash in advtrains before adding recipe
fsr.register_craft_if_items_exist({
    output = "advtrains:dtrack_atc_placer",
    recipe = {{"dye:blue"},
              {"default:mese_crystal_fragment"},
              {"advtrains:dtrack_placer"}}
    })
]]--
for i,color in ipairs(greens) do
    fsr.register_craft_if_items_exist({
        output = "advtrains:dtrack_unload_placer",
        recipe = {{color},
                  {"default:mese_crystal_fragment"},
                  {"advtrains:dtrack_placer"}}
        })
end
fsr.register_craft_if_items_exist({
    output = "advtrains:dtrack_load_placer",
    recipe = {{"dye:red"},
              {"default:mese_crystal_fragment"},
              {"advtrains:dtrack_placer"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:dtrack_load_placer",
    recipe = {{"advtrains:dtrack_unload_placer"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:dtrack_unload_placer",
    recipe = {{"advtrains:dtrack_load_placer"}}
    })

fsr.register_craft_if_items_exist({
    output = "advtrains_luaautomation:dtrack_placer",
    recipe = {{"dye:blue"},
              {"mesecons_microcontroller:microcontroller0000"},
              {"advtrains:dtrack_placer"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains_luaautomation:oppanel",
    recipe = {
        {"default:steel_ingot","dye:blue","default:steel_ingot"},
        {"default:steel_ingot","mesecons_microcontroller:microcontroller0000","default:steel_ingot"}}
    })


-- train stuff

fsr.register_craft_if_items_exist({
    output = "advtrains:engine_japan",
    recipe = {{"advtrains:wagon_japan"},
              {"default:mese"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:wagon_japan",
    recipe = {
        {"default:steel_ingot","default:steel_ingot","default:steel_ingot"},
        {"default:steel_ingot","advtrains:subway_wagon","default:steel_ingot"},
        {"default:steel_ingot","default:steel_ingot","default:steel_ingot"}}
    })

fsr.register_craft_if_items_exist({
    output = "advtrains:engine_industrial",
    recipe = {
        {"dye:red", "advtrains:driver_cab"},
        {"default:steelblock", "default:mese_crystal"},
        {"advtrains:wheel", "advtrains:wheel"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:engine_industrial_big",
    recipe = {
        {"dye:red", "advtrains:driver_cab", "default:steelblock"},
        {"default:steelblock", "default:mese", "default:steelblock"},
        {"advtrains:wheel", "advtrains:wheel", "advtrains:wheel"}}
    })

fsr.register_craft_if_items_exist({
    output = "advtrains:wagon_wood",
    recipe = {
        {"default:steel_ingot","","default:steel_ingot"},
        {"default:steel_ingot","default:steel_ingot","default:steel_ingot"},
        {"advtrains:wheel","advtrains:wheel","advtrains:wheel"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:wagon_tank",
    recipe = {
        {"default:steel_ingot","default:steel_ingot","default:steel_ingot"},
        {"default:steel_ingot","default:steel_ingot","default:steel_ingot"},
        {"advtrains:wheel","advtrains:wheel","advtrains:wheel"}}
    })

fsr.register_craft_if_items_exist({
    output = "advtrains:engine_railbus",
    recipe = {
        {"","basic_materials:steel_bar"},
        {"dye:red","advtrains:subway_wagon"}}
    })

fsr.register_craft_if_items_exist({
    output = "advtrains:diesel_lokomotive",
    recipe = {
        {"dye:cyan", "advtrains:driver_cab"},
        {"default:steelblock", "default:mese_crystal"},
        {"advtrains:wheel", "advtrains:wheel"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:wagon_track",
    recipe = {
        {"advtrains:dtrack_placer", "advtrains:dtrack_placer", "advtrains:dtrack_placer"},
        {"default:stick", "default:stick", "default:stick"},
        {"advtrains:wheel", "", "advtrains:wheel"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:wagon_gravel",
    recipe = {
        {"group:wood", "default:gravel", "group:wood"},
        {"group:wood", "group:wood", "group:wood"},
        {"advtrains:wheel", "", "advtrains:wheel"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:wagon_tree",
    recipe = {
        {"", "group:tree", ""},
        {"default:stick", "default:stick", "default:stick"},
        {"advtrains:wheel", "", "advtrains:wheel"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:wagon_lava",
    recipe = {
        {"default:stick", "bucket:bucket_lava", "default:stick"},
        {"group:wood", "group:wood", "group:wood"},
        {"advtrains:wheel", "", "advtrains:wheel"}}
    })

fsr.register_craft_if_items_exist({
    output = "advtrains:SaHa_E231",
    recipe = {
        {"default:steel_ingot","default:steel_ingot","default:steel_ingot"},
        {"default:steel_ingot","dye:green","default:steel_ingot"},
        {"advtrains:wheel","","advtrains:wheel"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:MoHa_E230",
    recipe = {
        {"default:steel_ingot","default:steel_ingot","default:steel_ingot"},
        {"default:steel_ingot","dye:green","default:steel_ingot"},
        {"advtrains:wheel","default:mese","advtrains:wheel"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:MoHa_E231",
    recipe = {
        {"basic_materials:steel_bar"},
        {"advtrains:MoHa_E230"}},
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:KuHa_E231",
    recipe = {
        {"default:steel_ingot","default:steel_ingot","default:steel_ingot"},
        {"default:glass","dye:green","default:steel_ingot"},
        {"advtrains:wheel","default:mese","advtrains:wheel"}}
    })

fsr.register_craft_if_items_exist({
    output = "advtrains:under_s7dm",
    recipe = {
        {"default:steel_ingot","default:steel_ingot","default:steel_ingot"},
        {"default:steel_ingot","dye:red","default:steel_ingot"},
        {"advtrains:wheel","dye:blue","advtrains:wheel"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:under_s7ndm",
    recipe = {
        {"default:steel_ingot","default:steel_ingot","default:steel_ingot"},
        {"default:steel_ingot","dye:red","default:glass"},
        {"advtrains:wheel","default:mese","advtrains:wheel"}}
    })


-- signal stuff

fsr.register_craft_if_items_exist({
    output = "advtrains_signals_ks:mast_mast_0",
    recipe = {{"default:steel_ingot"},
        {"default:steel_ingot"},
        {"default:steel_ingot"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains_signals_ks:hs_danger_0 2",
    type = "shapeless",
    recipe = {"advtrains:signal_off", "advtrains:signal_off", "dye:white", "dye:yellow"}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains_signals_ks:ra_danger_0 2",
    type = "shapeless",
    recipe = {"advtrains:signal_off", "advtrains:signal_off", "dye:white"}
    })

fsr.register_craft_if_items_exist({
    output = "advtrains:signal_wall_l_off",
    recipe = {{"advtrains:signal_wall_t_off"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:signal_wall_r_off",
    recipe = {{"advtrains:signal_wall_l_off"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains:signal_wall_t_off",
    recipe = {{"advtrains:signal_wall_r_off"}}
    })

for i,color in ipairs(greens) do
    fsr.register_craft_if_items_exist({
        output = "advtrains:signal_wall_l_off 2",
        recipe = {{"default:steel_ingot", "dye:red"},
            {"", color},
            {"default:steel_ingot", "default:steel_ingot"}}
        })
    fsr.register_craft_if_items_exist({
        output = "advtrains:signal_wall_r_off 2",
        recipe = {{"dye:red", "default:steel_ingot"},
            {color, ""},
            {"default:steel_ingot", "default:steel_ingot"}}
        })
    fsr.register_craft_if_items_exist({
        output = "advtrains:signal_wall_t_off 2",
        recipe = {{"default:steel_ingot", "", "default:steel_ingot"},
            {"default:steel_ingot", color, "dye:red"}}
        })
end

fsr.register_craft_if_items_exist({
    output = "advtrains_signals_ks:sign_8_0 2",
    type = "shapeless",
    recipe = {"default:sign_wall_steel", "default:sign_wall_steel", "default:steel_ingot", "dye:white", "dye:black"}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains_signals_ks:sign_8_0 2",
    type = "shapeless",
    recipe = {"basic_signs:sign_wall_steel_white_black", "default:steel_ingot"}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains_signals_ks:sign_8_0 2",
    recipe = {{"default:steel_ingot", "default:steel_ingot", "default:steel_ingot"},
        {"default:steel_ingot", "default:steel_ingot", "default:steel_ingot"},
        {"dye:black", "default:steel_ingot", "dye:white"}}
    })
fsr.register_craft_if_items_exist({
    output = "advtrains_signals_ks:sign_8_0 2",
    recipe = {{"default:steel_ingot", "default:steel_ingot", "default:steel_ingot"},
        {"default:steel_ingot", "default:steel_ingot", "default:steel_ingot"},
        {"dye:white", "default:steel_ingot", "dye:black"}}
    })



for rot = 0, 3 do
  local pos = function(n)
    if n == rot then
      return "dye:white"
    else
      return "dye:red"
    end
  end
  fsr.register_craft_if_items_exist({
    output = "advtrains:across_off 2",
    recipe = {
        {pos(3), "", pos(2)},
        {pos(0), "default:mese_crystal_fragment", pos(1)},
        {"", "default:steel_ingot", ""}}
    })
end


-- add recipe with normal green for signal
fsr.register_craft_if_items_exist({
    output = "advtrains:signal_off 2",
    recipe = {
        {"", "dye:red", "default:steel_ingot"},
        {"", "dye:green", "default:steel_ingot"},
        {"", "", "default:steel_ingot"},
    }
})
