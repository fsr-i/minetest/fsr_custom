local DISABLE_TIMEOUT = tonumber(minetest.settings:get("fsr_custom.advtrains.disable_timeout")) or 60 * 60

local trains_disabled = #minetest.get_connected_players() == 0
local disable_job = nil


minetest.register_on_joinplayer(function(player, last_login)
	-- activate trains
	if disable_job ~= nil then
		disable_job:cancel()
		disable_job = nil
	end
	if trains_disabled then
		minetest.log("action", "[fsr_custom] Enabling trains.")
		trains_disabled = false
	end
end)

minetest.register_on_leaveplayer(function(player, timed_out)
	if #minetest.get_connected_players() <= 1 then	-- leaving player is still counted
		-- deactivate trains
		if disable_job == nil then
			disable_job = minetest.after(DISABLE_TIMEOUT, function()
				minetest.log("action", "[fsr_custom] No player logged in for a time. Disabling trains.")
				trains_disabled = true
				disable_job = nil
			end)
		end
	end
end)


-- overrides

assert(type(advtrains.train_ensure_init) == "function")
local old_train_ensure_init = advtrains.train_ensure_init
advtrains.train_ensure_init = function(k, v)
	if trains_disabled then
		return
	else
		return old_train_ensure_init(k, v)
	end
end

assert(type(advtrains.train_step_b) == "function")
local old_train_step_b = advtrains.train_step_b
advtrains.train_step_b = function(k, v, dtime)
	if trains_disabled then
		return
	else
		return old_train_step_b(k, v, dtime)
	end
end

assert(type(advtrains.train_step_c) == "function")
local old_train_step_c = advtrains.train_step_c
advtrains.train_step_c = function(k, v, dtime)
	if trains_disabled then
		return
	else
		return old_train_step_c(k, v, dtime)
	end
end
