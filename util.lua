
fsr.get_item_name_from_string = function(str)
	local space = string.find(str, ' ')
	if space == nil then
		return str
	else
		return string.sub(str, 1, space-1)
	end
end

fsr.is_group_string = function(str)
	local group = 'group:'
	if string.sub(str, 1, #group) == group then
		return string.sub(str, #group+1)
	else
		return false
	end
end


local debug = function(msg, item)
	if item == nil then
		item = ''
	else
		item = " " .. fsr.get_item_name_from_string(tostring(item))
	end
	print("fsr.register_craft_if_items_exist() "..msg..item)
end


fsr.discovered_item_groups = {}

fsr.group_exists = function(group)
	-- all desired items have to have been registered already to discover their groups (obviously)
	-- you might need to include these mods as optional dependencies
	if fsr.discovered_item_groups[group] == group then
		return true
	end

	for name, item in pairs(minetest.registered_items) do
		if type(item.groups) == "table" then
			for group, rating in pairs(item.groups) do
				fsr.discovered_item_groups[group] = group
			end
		end
		if fsr.discovered_item_groups[group] == group then
			local n = 0
			for k,v in pairs(fsr.discovered_item_groups) do
				n = n+1
			end
			return true
		end
	end
	return false
end


fsr.register_craft_if_items_exist = function(def)
	if def.output and minetest.registered_items[fsr.get_item_name_from_string(def.output)] == nil then
		debug("[FAIL] unknown output", def.output)
		return false
	end
	if def.recipe then
		if type(def.recipe) ~= "table" then
			local recipe_item = fsr.get_item_name_from_string(def.recipe)
			if minetest.registered_items[recipe_item] == nil then
				local group = fsr.is_group_string(recipe_item)
				if not (group and fsr.group_exists(group)) then
					debug("[FAIL] unknown recipe", recipe_item)
					return false
				end
			end
		else
			for y, row in ipairs(def.recipe) do
				if type(row) ~= "table" then
					local row_item = fsr.get_item_name_from_string(row)
					if minetest.registered_items[row_item] == nil then
						local group = fsr.is_group_string(row_item)
						if not (group and fsr.group_exists(group)) then
							debug("[FAIL] unknown row item", row_item)
							return false
						end
					end
				else
					for x, col in ipairs(row) do
						local col_item = fsr.get_item_name_from_string(col)
						if col ~= "" and minetest.registered_items[col_item] == nil then
							local group = fsr.is_group_string(col_item)
							if not (group and fsr.group_exists(group)) then
								debug("[FAIL] unknown item", col)
								return false
							end
						end
					end
				end
			end
		end
		-- still here? recipe is fine then!
		minetest.register_craft(def)
		return true
	end
end
