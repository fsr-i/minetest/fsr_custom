
fsr.register_craft_if_items_exist({
    output = "digilines:lightsensor",
    recipe = {
        {"basic_materials:plastic_sheet", "xpanes:pane_flat", "basic_materials:plastic_sheet"},
        {"default:steel_ingot", "mesecons_luacontroller:luacontroller0000", "digilines:wire_std_00000000"}
    }
})
fsr.register_craft_if_items_exist({
    output = "digilines:rtc",
    recipe = {
        {"basic_materials:plastic_sheet", "basic_materials:energy_crystal_simple", "basic_materials:plastic_sheet"},
        {"default:steel_ingot", "mesecons_luacontroller:luacontroller0000", "digilines:wire_std_00000000"}
    }
})

fsr.register_craft_if_items_exist({
    output = "computer:tetris_arcade",
    recipe = {
        {"basic_materials:plastic_sheet", "basic_materials:energy_crystal_simple", "basic_materials:plastic_sheet"},
        {"dye:black", "default:glass", "dye:black"},
        {"basic_materials:plastic_sheet", "basic_materials:energy_crystal_simple", "basic_materials:plastic_sheet"}
    }
})
fsr.register_craft_if_items_exist({
    output = "computer:3dprinter_bedflinger",
    recipe = {
        {"basic_materials:plastic_sheet", "basic_materials:empty_spool", "default:mese_crystal_fragment"},
        {"basic_materials:plastic_sheet", "", "basic_materials:plastic_sheet"},
        {"default:steel_ingot", "default:steel_ingot", "default:steel_ingot"}
    }
})
